﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using System.Xml.Linq;
using System.Device.Location;
using Microsoft.Devices.Sensors;
using System.Collections.ObjectModel;
using GART.Data;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using Microsoft.Phone.Shell;

namespace MuseosD.FMetroStyle
{
    public partial class PivotPage1 : PhoneApplicationPage
    {
        SaveFile file = new SaveFile();
        private readonly CredentialsProvider _credentialsProvider = new ApplicationIdCredentialsProvider("AlWUrDyAiFaD2vaFWYOSJNzYT7vI11eo9VqpUxnHlX4yhyesP3Mh5Mht4exuRf4z");
        List<MuseosDetalle> ListaDatos = new List<MuseosDetalle>();
        public static List<MuseosDetalle> ListaItemAR = new List<MuseosDetalle>();
        public GeoCoordinate myloc = new GeoCoordinate();
        private ObservableCollection<ARItem> locations;
        //Compass brujula;
        GeoCoordinateWatcher watcher = null;
        double[] distanceKm = new double[35];
        int id;
        string urlCompartir;
        string nombreMuseo;

        //AppBar
        ApplicationBarIconButton button1 = new ApplicationBarIconButton();
        ApplicationBarIconButton button2 = new ApplicationBarIconButton();
        ApplicationBarIconButton button3 = new ApplicationBarIconButton();
        ApplicationBarIconButton button4 = new ApplicationBarIconButton();
        ApplicationBarMenuItem menuItem1 = new ApplicationBarMenuItem();

        public PivotPage1()
        {
            InitializeComponent();
            map1.CredentialsProvider = _credentialsProvider;
            localizar();
            locations = new ObservableCollection<ARItem>();
        }

        void Parser_info()
        {
            XDocument xmlListaMuseos = XDocument.Parse(file.obtener_file("MuseosDF.xml"));


            var info = from list in xmlListaMuseos.Descendants("item")
                                     where (list.Element("id").Value == MainPage.identificador)
                                     select new MuseosDetalle
                                     {
                                         nombre = list.Element("nombre").Value,
                                         imagen = list.Element("imagen").Value,
                                         descripcion = list.Element("descripcion").Value,
                                         id = list.Element("id").Value,
                                         horario = list.Element("horario").Value,
                                         costos = list.Element("costos").Value,
                                         promociones = list.Element("promociones").Value,
                                         servicios = list.Element("servicios").Value,
                                         url = list.Element("url").Value,
                                         direccion = list.Element("direccion").Value,
                                         latitud = Convert.ToDouble(list.Element("latitud").Value),
                                         longitud = Convert.ToDouble(list.Element("longitud").Value)
                                     };

            ListaDatos = (info as IEnumerable<MuseosDetalle>).ToList();

            nombreMuseo = ListaDatos.ElementAt(0).nombre;
            Direccion.Text = ListaDatos.ElementAt(0).direccion;
            DesMuseo.Text = ListaDatos.ElementAt(0).descripcion;
            BitmapImage imagenSource = new BitmapImage(new Uri(ListaDatos.ElementAt(0).imagen, UriKind.RelativeOrAbsolute));
            image1.Source = imagenSource;
            Costos.Text = ListaDatos.ElementAt(0).costos + " " + "Promociones: " + ListaDatos.ElementAt(0).promociones;
            horario.Text = ListaDatos.ElementAt(0).horario;
            servicios.Text = ListaDatos.ElementAt(0).servicios;
            urlCompartir = ListaDatos.ElementAt(0).url;
            map1.Center = new GeoCoordinate(ListaDatos.ElementAt(0).latitud, ListaDatos.ElementAt(0).longitud);
            map1.ZoomLevel = 14;
            Pushpin pin1 = new Pushpin();
            pin1.Location = new GeoCoordinate(ListaDatos.ElementAt(0).latitud, ListaDatos.ElementAt(0).longitud);
            pin1.Content = ListaDatos.ElementAt(0).nombre;
            map1.Children.Add(pin1);
            progressBar1.Visibility = System.Windows.Visibility.Collapsed;
            progressBar3.Visibility = System.Windows.Visibility.Collapsed;
            progressBar5.Visibility = System.Windows.Visibility.Collapsed;
            Parser_AR();
        }

        void Parser_AR()
        {
            /*********************************************AR ITEMS***********************************************/
            XDocument xmlListaMuseos = XDocument.Parse(file.obtener_file("MuseosDF.xml"));

            var info2 = from list in xmlListaMuseos.Descendants("item")
                        where (list.Element("categoria").Value == MainPage.idcategoria)
                        select new MuseosDetalle
                        {
                            nombre = list.Element("nombre").Value,
                            imagen = list.Element("imagen").Value,
                            id = list.Element("id").Value,
                            horario = list.Element("horario").Value,
                            direccion = list.Element("direccion").Value,
                            latitud = Convert.ToDouble(list.Element("latitud").Value),
                            longitud = Convert.ToDouble(list.Element("longitud").Value)
                        };

            ListaItemAR = (info2 as IEnumerable<MuseosDetalle>).ToList();
            //tamañoElementos = ListaItemAR.Count;
            iniciar_AR();
        }

        void iniciar_AR()
        {
            double[] distancia = new double[ListaItemAR.Count];
            GeoCoordinate[] posicion = new GeoCoordinate[ListaItemAR.Count];
            GeoCoordinate[] sitiosLoc = new GeoCoordinate[ListaItemAR.Count];
            GeoCoordinate[] trueposicion = new GeoCoordinate[ListaItemAR.Count];
            double[] restaLat = new double[ListaItemAR.Count];
            double[] restaLon = new double[ListaItemAR.Count];
            double[] proporcionLat = new double[ListaItemAR.Count];
            double[] proporcionLon = new double[ListaItemAR.Count];
            //double[] distanceKm = new double[ListaItemAR.Count];

            try
            {

                for (int i = 0; i < ListaItemAR.Count; i++)
                {

                    sitiosLoc[i] = new GeoCoordinate(ListaItemAR.ElementAt(i).latitud, ListaItemAR.ElementAt(i).longitud);
                    posicion[i] = new GeoCoordinate(myloc.Latitude, myloc.Longitude);
                    distancia[i] = Math.Round(posicion[i].GetDistanceTo(sitiosLoc[i]), 2);

                    distanceKm[i] = distancia[i];

                    restaLat[i] = posicion[i].Latitude - ListaItemAR.ElementAt(i).latitud;
                    restaLon[i] = posicion[i].Longitude - ListaItemAR.ElementAt(i).longitud;

                    proporcionLat[i] = (restaLat[i] * 100) / distancia[i] * -1;
                    proporcionLon[i] = (restaLon[i] * 100) / distancia[i] * -1;

                    trueposicion[i] = new GeoCoordinate(posicion[i].Latitude + proporcionLat[i], posicion[i].Longitude + proporcionLon[i]);

                    locations.Add(new MuseosDFItemAR()
                    {
                        GeoLocation = trueposicion[i],
                        Nombre = ListaItemAR.ElementAt(i).nombre,
                        Id = i
                    });
                }

                ARDisplay.ARItems = locations;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Falló al obtener datos intenta reiniciando la aplicación");
            }

            progressBar2.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void localizar()
        {
            AppBar();

            try
            {

                watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
                watcher.MovementThreshold = 20;
                watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(watcher_StatusChanged);
                watcher.Start();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Falló al iniciar el GPS intenta de nuevo");
            }
        }

        void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {

            try
            {

                switch (e.Status)
                {
                    case GeoPositionStatus.Disabled:
                        // The Location Service is disabled or unsupported.
                        // Check to see whether the user has disabled the Location Service.
                        if (watcher.Permission == GeoPositionPermission.Denied)
                        {
                            // The user has disabled the Location Service on their device.
                            MessageBox.Show("Uso de Ubicación desactivada ve a configuraciones para activar.");
                        }
                        else
                        {
                            MessageBox.Show("Localización no esta funcionando en este dispositivo");
                        }

                        break;

                    case GeoPositionStatus.Initializing:
                        progressBar1.Visibility = System.Windows.Visibility.Visible;
                        break;

                    case GeoPositionStatus.NoData:
                        // The Location Service is working, but it cannot get location data.
                        // Alert the user and enable the Stop Location button.
                        MessageBox.Show("Tu ubicación no esta disponible.");
                        break;

                    case GeoPositionStatus.Ready:
                        // The Location Service is working and is receiving location data.
                        // Show the current position and enable the Stop Location button.

                        var result = MessageBox.Show("La aplicación Museos D.F. requiere acceder a tu ubicación."
                                     + " Presiona Aceptar para permitir o" +
                                     " Cancelar para negar", 
                                     "Usar Ubicación",
                                     MessageBoxButton.OKCancel);
                        if (result == MessageBoxResult.Cancel)
                        {
                            MessageBox.Show("Uso de Ubicación desactivada");
                        }
                        else
                        {
                            watcher.Stop();
                            myloc = watcher.Position.Location;
                            button1.IsEnabled = true;
                            button2.IsEnabled = true;
                            button3.IsEnabled = true;
                            button4.IsEnabled = true;
                            menuItem1.IsEnabled = true;
                            ApplicationBar.Mode = Microsoft.Phone.Shell.ApplicationBarMode.Minimized;
                            progressBar1.Visibility = System.Windows.Visibility.Collapsed;
                            Parser_info();
                        }
                    break;
                }

            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Falló al intentar obtener ubicación cambia la región a México");
            }

        }

        private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Border infoEdi = sender as Border;
            //double[] distanceKm = new double[tamañoElementos];

            if (infoEdi != null)
            {
                if (infoEdi.Tag != null)
                {
                    id = Convert.ToInt32(infoEdi.Tag);
                }
            }

            try
            {
                double distanciaKm = Math.Round(distanceKm[id] / 1000, 2);

                sitio.Text = ListaItemAR.ElementAt(id).nombre + " Te encuentras a: " + distanciaKm.ToString() + " kilometros";
                description.Text = ListaItemAR.ElementAt(id).direccion + " " + ListaItemAR.ElementAt(id).horario;
                info.Visibility = System.Windows.Visibility.Visible;
            }

            catch (Exception ex)
            {
                MessageBox.Show("No se pudo calcular la distancia");
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                ARDisplay.StartServices();
                base.OnNavigatedTo(e);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Falló el módulo de Realidad Aumentada intenta reiniciando la aplicación");
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                // Stop AR services
                ARDisplay.StopServices();
                base.OnNavigatedFrom(e);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Falló el módulo de Realidad Aumentada intenta reiniciando la aplicación");
            }
        }

        private void appbar_button1_Click(object sender, EventArgs e)
        {
            try
            {
                map1.Center = new GeoCoordinate(myloc.Latitude, myloc.Longitude);
                Pushpin pushpinMyLoc = new Pushpin();
                pushpinMyLoc.Location = myloc;
                pushpinMyLoc.Content = "YO";
                map1.Children.Add(pushpinMyLoc);
            }

            catch (Exception ex)
            {
                MessageBox.Show("No te podemos ubicar");
            }
        }

        private void appbar_button2_Click(object sender, EventArgs e)
        {
            try
            {
                BingMapsDirectionsTask bingMapsDirectionsTask = new BingMapsDirectionsTask();

                // You can specify a label and a geocoordinate for the end point.
                GeoCoordinate spaceNeedleLocation = new GeoCoordinate(ListaDatos.ElementAt(0).latitud, ListaDatos.ElementAt(0).longitud);
                LabeledMapLocation spaceNeedleLML = new LabeledMapLocation(ListaDatos.ElementAt(0).nombre, spaceNeedleLocation);
                bingMapsDirectionsTask.End = spaceNeedleLML;

                // If bingMapsDirectionsTask.Start is not set, the user's current location is used as the start point.

                bingMapsDirectionsTask.Show();
            }

            catch (Exception ex)
            {
                MessageBox.Show("No se puede obtener ruta");
            }
            
        }

        private void appbar_button3_Click(object sender, EventArgs e)
        {
            if (map1.Mode is AerialMode)
            {
                map1.Mode = new RoadMode();
            }
            else
            {
                map1.Mode = new AerialMode(true);
            }
        }

        private void appbar_button4_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Info.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItem1_Click(object sender, EventArgs e)
        {
            ShareStatusTask shareStatusTask = new ShareStatusTask();
            shareStatusTask.Status = "Te recomiendo Visitar el " + nombreMuseo + "-->" + urlCompartir 
                + " Publicado desde: Museos D.F (Para WP7)";
            shareStatusTask.Show();
            //NavigationService.Navigate(new Uri("/PanoramaPage1.xaml", UriKind.Relative));
        }

        void AppBar()
        {

            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;
            ApplicationBar.Opacity = 0.8;
            ApplicationBar.IsVisible = true;

            button1.IconUri = new Uri("/Imagenes/yo.png", UriKind.Relative);
            button1.Text = "Yo";
            button1.IsEnabled = false;
            ApplicationBar.Buttons.Add(button1);
            button1.Click += new EventHandler(appbar_button1_Click);

            button2.IconUri = new Uri("/Imagenes/ruta.png", UriKind.Relative);
            button2.Text = "Ruta";
            button2.IsEnabled = false;
            ApplicationBar.Buttons.Add(button2);
            button2.Click += new EventHandler(appbar_button2_Click);

            button3.IconUri = new Uri("/Imagenes/tipoMap.png", UriKind.Relative);
            button3.Text = "TipoMapa";
            button3.IsEnabled = false;
            ApplicationBar.Buttons.Add(button3);
            button3.Click += new EventHandler(appbar_button3_Click);

            button4.IconUri = new Uri("/Imagenes/info.png", UriKind.Relative);
            button4.Text = "info";
            button4.IsEnabled = false;
            ApplicationBar.Buttons.Add(button4);
            button4.Click += new EventHandler(appbar_button4_Click);

            menuItem1.Text = "Compartir";
            menuItem1.IsEnabled = false;
            ApplicationBar.MenuItems.Add(menuItem1);
            menuItem1.Click += new EventHandler(ApplicationBarMenuItem1_Click);

        }
    }
}