﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MuseosD.FMetroStyle
{
    public class MuseosDetalle
    {
        public string id { get; set; }
        public string categoria { get; set; }
        public string nombre { get; set; }
        public string imagen { get; set; }
        public string descripcion { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public string direccion { get; set; }
        public string url { get; set; }
        public string contacto { get; set; }
        public string horario { get; set; }
        public string costos { get; set; }
        public string promociones { get; set; }
        public string servicios { get; set; }
    }
}
