﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;

namespace MuseosD.FMetroStyle
{
    public class SaveFile
    {
        string archivo;

        public SaveFile()
        {
        }

        #region File

        public void guardarXml(string datos, string name)
        {

            try
            {
                IsolatedStorageFile fileStorage = IsolatedStorageFile.GetUserStoreForApplication();
                StreamWriter fileWriter;

                if (!string.IsNullOrEmpty("xmlFiles") && !fileStorage.DirectoryExists("xmlFiles"))
                {
                    fileStorage.CreateDirectory("xmlFiles");
                    fileWriter = new StreamWriter(new IsolatedStorageFileStream("xmlFiles\\MuseosDF" + name, FileMode.OpenOrCreate, fileStorage));
                    fileWriter.Write(datos);
                    fileWriter.Close();
                }

                else
                {
                    fileStorage.DeleteFile("xmlFiles\\MuseosDF" + name);
                    fileWriter = new StreamWriter(new IsolatedStorageFileStream("xmlFiles\\MuseosDF" + name, FileMode.OpenOrCreate, fileStorage));
                    fileWriter.Write(datos);
                    fileWriter.Close();
                }

            }

            catch (IsolatedStorageException e)
            {
                //MessageBox.Show(e.Message);
                MessageBox.Show("No se pudo guardar el contenido");
            }

        }

        public string obtener_file(string name)
        {

            try
            {
                IsolatedStorageFile fileStorage = IsolatedStorageFile.GetUserStoreForApplication();
                StreamReader fileReader = null;

                fileReader = new StreamReader(new IsolatedStorageFileStream("xmlFiles\\MuseosDF" + name, FileMode.Open, fileStorage));

                archivo = fileReader.ReadToEnd();

                fileReader.Close();
            }

            catch (IsolatedStorageException ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("No se pudo acceder a los datos del dispositivo");
            }

            return archivo;
        }

        #endregion
    }
}
