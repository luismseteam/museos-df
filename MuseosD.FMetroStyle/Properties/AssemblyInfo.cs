﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// La información general de un ensamblado se controla mediante el siguiente 
// conjunto de atributos. Cambie los valores de estos atributos para modificar la información
// asociada a un ensamblado.
[assembly: AssemblyTitle("MuseosD.FMetroStyle")]
[assembly: AssemblyDescription("Descubre los museos del D.F de una manera sorprendente con realidad aumentada")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("UNAM Mobile")]
[assembly: AssemblyProduct("MuseosD.FMetroStyle")]
[assembly: AssemblyCopyright("Copyright ©  2012")]
[assembly: AssemblyTrademark("UNAM Mobile")]
[assembly: AssemblyCulture("")]

// Si ComVisible se establece en False, los componentes COM no verán los 
// tipos de este ensamblado. Si necesita obtener acceso a un tipo de este ensamblado desde 
// COM, establezca el atributo ComVisible en True en este tipo.
[assembly: ComVisible(false)]

// El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
[assembly: Guid("cfb6b624-4415-467f-a3ea-6f6eb70d29d2")]

// La información de versión de un ensamblado consta de los cuatro valores siguientes:
//
//      Versión principal
//      Versión secundaria 
//      Número de compilación
//      Revisión
//
// Puede especificar todos los valores o usar los valores predeterminados de número de compilación y revisión 
// mediante el carácter '*', como se muestra a continuación:
[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
[assembly: NeutralResourcesLanguageAttribute("es-MX")]
