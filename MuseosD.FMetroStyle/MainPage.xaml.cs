﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Net.NetworkInformation;
using System.Xml.Linq;

namespace MuseosD.FMetroStyle
{
    public partial class MainPage : PhoneApplicationPage
    {
        SaveFile file = new SaveFile();
        public static List<MuseosDetalle> ListaMuseosPrehisColo = new List<MuseosDetalle>();
        public static List<MuseosDetalle> ListaMuseosModerContem = new List<MuseosDetalle>();
        public static List<MuseosDetalle> ListaMuseosCienciaTec = new List<MuseosDetalle>();
        public static List<MuseosDetalle> ListaMuseosHistoria = new List<MuseosDetalle>();
        public static string identificador;
        public static string idcategoria;

        public MainPage()
        {
            InitializeComponent();
            doGet();
        }

        public void doGet()
        {

            if (NetworkInterface.GetIsNetworkAvailable())
            {
                WebClient xmlCategorias = new WebClient();
                xmlCategorias.DownloadStringCompleted += new DownloadStringCompletedEventHandler(xml_Categorias);
                xmlCategorias.DownloadStringAsync(new Uri("http://nokiapps.unam.mobi/services/museos"));
            }

            else
            {
                if (ValidarFile() == true)
                {
                    Parser();
                }

                else
                {
                    MessageBox.Show("No hay datos almacenados y no hay conexión a internet inicie la aplicación al menos 1 vez con conexión");
                }
            }

        }

        void xml_Categorias(object sender, DownloadStringCompletedEventArgs e)
        {

            if (e.Error != null || e.Cancelled)
            {
                MessageBox.Show("Sin Conexión intenta mas tarde");
                return;
            }

            string datos = e.Result;

            file.guardarXml(datos, "MuseosDF.xml");
            Parser();
        }

        public bool ValidarFile()
        {

            string prueba = file.obtener_file("MuseosDF.xml");

            if (prueba == null)
            {
                return false;
            }

            else
            {
                return true;
            }

        }

        void Parser()
        {

            /***********************************lista 1*************************************/

            XDocument xmlListaMuseos = XDocument.Parse(file.obtener_file("MuseosDF.xml"));

            PrehisColo.ItemsSource = from list in xmlListaMuseos.Descendants("item")
                                     where (list.Element("categoria").Value == "195")
                                     select new MuseosDetalle
                                     {
                                         nombre = list.Element("nombre").Value,
                                         id = list.Element("id").Value,
                                     };

            ListaMuseosPrehisColo = (PrehisColo.ItemsSource as IEnumerable<MuseosDetalle>).ToList();

            /***********************************lista 2*************************************/

            XDocument xmlListaMuseos2 = XDocument.Parse(file.obtener_file("MuseosDF.xml"));

            ModerContem.ItemsSource = from list in xmlListaMuseos2.Descendants("item")
                                     where (list.Element("categoria").Value == "196")
                                     select new MuseosDetalle
                                     {
                                         nombre = list.Element("nombre").Value,
                                         id = list.Element("id").Value,
                                     };

            ListaMuseosModerContem = (ModerContem.ItemsSource as IEnumerable<MuseosDetalle>).ToList();


            /***********************************lista 3*************************************/

            XDocument xmlListaMuseos3 = XDocument.Parse(file.obtener_file("MuseosDF.xml"));

            CienciaTec.ItemsSource = from list in xmlListaMuseos3.Descendants("item")
                                     where (list.Element("categoria").Value == "197")
                                     select new MuseosDetalle
                                     {
                                         nombre = list.Element("nombre").Value,
                                         id = list.Element("id").Value,
                                     };

            ListaMuseosCienciaTec = (CienciaTec.ItemsSource as IEnumerable<MuseosDetalle>).ToList();

            /***********************************lista 4*************************************/

            XDocument xmlListaMuseos4 = XDocument.Parse(file.obtener_file("MuseosDF.xml"));

            Historia.ItemsSource = from list in xmlListaMuseos4.Descendants("item")
                                     where (list.Element("categoria").Value == "198")
                                     select new MuseosDetalle
                                     {
                                         nombre = list.Element("nombre").Value,
                                         id = list.Element("id").Value,

                                     };

            ListaMuseosHistoria = (Historia.ItemsSource as IEnumerable<MuseosDetalle>).ToList();
            progressBar1.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void PrehisColo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PrehisColo.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identificador = ListaMuseosPrehisColo.ElementAt(PrehisColo.SelectedIndex).id;
            idcategoria = "195";

            NavigationService.Navigate(new Uri("/PivotPage1.xaml", UriKind.Relative));
            // Reset selected index to -1 (no selection)
            PrehisColo.SelectedIndex = -1;
        }

        private void ModerContem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ModerContem.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identificador = ListaMuseosModerContem.ElementAt(ModerContem.SelectedIndex).id;
            idcategoria = "196";

            NavigationService.Navigate(new Uri("/PivotPage1.xaml", UriKind.Relative));
            // Reset selected index to -1 (no selection)
            ModerContem.SelectedIndex = -1;
        }

        private void CienciaTec_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CienciaTec.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identificador = ListaMuseosCienciaTec.ElementAt(CienciaTec.SelectedIndex).id;
            idcategoria = "197";

            NavigationService.Navigate(new Uri("/PivotPage1.xaml", UriKind.Relative));
            // Reset selected index to -1 (no selection)
            CienciaTec.SelectedIndex = -1;
        }

        private void Historia_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Historia.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identificador = ListaMuseosHistoria.ElementAt(Historia.SelectedIndex).id;
            idcategoria = "198";

            NavigationService.Navigate(new Uri("/PivotPage1.xaml", UriKind.Relative));
            // Reset selected index to -1 (no selection)
            Historia.SelectedIndex = -1;
        }
    }
}