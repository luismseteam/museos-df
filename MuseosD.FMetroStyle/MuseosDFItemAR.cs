﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using GART.Data;

namespace MuseosD.FMetroStyle
{
    public class MuseosDFItemAR : ARItem
    {
        private string nombre;
        //private string urlicon;
        private int id;

        /*
        public string Urlicon
        {
            get
            {
                return urlicon;
            }
            set
            {
                if (urlicon != value)
                {
                    urlicon = value;
                    NotifyPropertyChanged(() => Urlicon);
                }
            }
        }*/

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (id != value)
                {
                    id = value;
                    NotifyPropertyChanged(() => id);
                }
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (nombre != value)
                {
                    nombre = value;
                    NotifyPropertyChanged(() => Nombre);
                }
            }
        }
    }
}
